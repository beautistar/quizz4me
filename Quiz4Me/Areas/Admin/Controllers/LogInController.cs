﻿using Quiz4Me.Areas.Admin.Models;
using Quiz4Me.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Quiz4Me.Areas.Admin.Controllers
{
    public class LogInController : Controller
    {
        private UserService _userService = new UserService();
        // GET: Admin/LogIn
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogIn()
        {
            LogInViewModel viewModel = new LogInViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult LogIn(LogInViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            if (_userService.IsValidUser(model.UserName, model.Password))
            {
                return RedirectToAction("index");
            }

            ModelState.AddModelError("Error", "Invalid Username and Password.");
            return View(model);
        }
    }
}