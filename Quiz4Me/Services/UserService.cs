﻿using Quiz4Me.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Quiz4Me.Services
{
    public class UserService
    {
        Quiz4MeEntities db = new Quiz4MeEntities();

        public bool IsValidUser(string UserName, string Password)
        {
            return db.Users.Where(x => x.UserName.ToLower() == UserName.ToLower() && x.Password.ToLower() == Password.ToLower()).Any();
        }
    }
}